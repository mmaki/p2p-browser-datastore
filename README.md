This project was a disaster but a very useful and interesting one!

If you remember my course project, I was setting about designing a 'P2P datastore' 
that I wanted to use for developing P2P applications. This is an extremely ambitious task 
and I decided to start by implementing the Byzantine Eventually Consistent broadcast 
mechanism that Martin Kleppmann describes here: https://arxiv.org/abs/2012.00472 but 
connection agnostic so I can simulate a network with threads on my own machine.
But I used a simple struct with a state-machine-like interface for the protocol, one method
per request type, and everything stored in a Peer struct's state. The complexity was unmanageable.
I tried a rewrite, it failed. So I switched gears (roughly 4 days ago) and decided to 
use Streams to make an entirely reactive API (A la The Reactive Manifesto) with just 2 core methods: 

* P2PEngine::listen_for<Message>() -> Stream<Message>
* P2PEngine::send(Message) -> Stream<RespMessage>. 

Usage would look like this:

```rust
//All code examples use a simplified psuedo code form of rust

let alice = P2PEngine::new();
alice.listen_for<Message>()
    .flat_map(|msg| alice.send(RespMessage{}))
    .inspect(|res| println!("Request-response! Yay!"));
alice.connect(/*Something*/);
```

I would use this asynchronous/functional style to define protocol flows implicitly. You could define
an encryption scheme with something like this:

```rust
fn encryption(other_protocol_stream: Stream<Message>) {
  let alice = P2PEngine::new();
  alice.listen_for<ReceivePublicKey>()
      .map(|msg| generate_shared_secret(msg.public_key))
      .flat_map(|encrypter| {
          other_protocol_stream
            .map(encrypter.encrypt_message)
      }); //Produces a stream of EncryptedMessage<Message> objects
}
```

Initially I tried to store the message formatter as a list of subtyped objects, like from my Java days.
Rust's generics stumped me: how could I produce messages of a specific type when that type had been erased 
during storage? After a little bit of thinking I realized that I only had 1 region of code where my P2PEngine
could know the type: Inside the body of `listen_for<T>`.

With this insight, it was surprisingly simple: create an async channel for a raw bit stream message (`InternalMessage`
in the code), store one end, and hook a filter onto the other. I decided to use failed parsing to indicate that this
message wasn't in the right channel. This could be made much more efficient and provide more features if I used a 
(Address, MessageFormat) pair as the key to the map storing the sender channels, but that's for 
later re-designs.

The big problem with this API was the P2PEngine::send() message. I spent hours trying to figure out how 
to create a one-shot listener for message responses when I realized: the channels automatically close themselves
when one end is dropped. I could just garbage-collect any disabled channels when I went through and tried to deliver
messages! I still had an issue where you might receive a different requests response, but I could fix that in a 
redesign by silently including a nonce or something. 

But this wasn't the only problem, remember the sample above:

```rust
let alice = P2PEngine::new(); //Owned outside of the stream
alice.listen_for<Message>()
    .flat_map(|msg| alice.send(RespMessage{})) //OWNERSHIP CHANGE TO INSIDE THE STREAM
    .inspect(|res| println!("Request-response! Yay!"));
```

Right at the end of this redesign process I realize I had shot myself in the foot with this API. Creating
an API like this necessarily means consuming the P2PEngine. I'm stumped.

But only for time! As I was writing my P2PEngine::connect() method yesterday, I realized that my current 
P2PEngine component attempts to do 3 things: 

* Manage a Public Key (see the constructor)
* Multiplex a channel
* Send messages over the wire

And this is why I was doing so much random stuff: spawning on a threadpool, trying to include a send message 
queue, opening files, etc. etc. All I need to do is decompose this object further, and It'll be great! I just
ran out of time to do it.

The last homework and this final project have made me love Rust even more. I'm finally starting to have a 
feel for the way different regions of code have their own context/flow and how ownership is really about crossing
over flow boundaries in a controlled way, and it's genuinely made me a better developer. My Java code wouldn't 
care about all of this reference movement, it can keep track of the object in a stream as well as anywhere else!
But Rust forces me to untangle my confused ideas and cleanly separate concerns. From the state machine -> streams, 
to the final realization that I still had no idea what I was even building... just a little bit of friction that the
ownership model creates has pushed my code into being tighter, simpler, and more goal oriented. truly an incredible 
language.

Final notes:

* I wrote 1 test for this code because I don't have a damn clue how to test reactive code. If this where a
  looser language I might look at dummy objects? Or maybe something else? I should figure this out if I want
  to use a reactive style long term.

* You may have noticed this program does nothing :(. I expect this is because I'm misusing the ThreadPool 
  on the futures::executor crate somehow and everything is dropping itself before other parts of the code 
  come online. It looks like I'll probably have to move to tokio to fix that, if only because they have
  better documentation I can use. Sorry! It's on the TODO list. Edit: I just ran this one more time, 
  and it actually produced a message randomly one time! This means it's definitely an 'dropping things too 
  quickly' problem. 

* I left my TODOs in as this is clearly a WIP, I won't pretend I've got anything clean and shiny to show off
  here.

* Damn, rust is neat!
  