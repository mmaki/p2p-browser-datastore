/// This will be an implementation of a shared secret generating and encryption protocol.
/// This is intended to box up other protocols and generate encrypted versions of those
/// protocols. Unfortunately, my API is not yet powerful enough to properly implement
/// This kind of behavior.
pub mod chacha_encrypt {
    use crate::peer::{Message, ParseMessageError};

    //TODO: Types are too limited for this kind of boxing and unboxing rn, re-work API
    struct EncryptedMessage<T: Message> {
        message: T,
    }

    /// These are stubs to get rid of compile errors. This is supposed to encrypt and decrypt messages
    /// then forward the actual parsing of the bits to the boxed message.
    impl<T: Message> Message for EncryptedMessage<T> {
        fn parse(data: Vec<u8>) -> Result<Box<EncryptedMessage<T>>, ParseMessageError> {
            let m = T::parse(data)?;

            Ok(Box::new(EncryptedMessage { message: *m }))
        }

        fn to_bytes(&self) -> Vec<u8> {
            self.message.to_bytes()
        }
    }

    // This would have used x25519_dalek to generate a shared secret and chacha20poly1305 encryption to
    // wrap the underlying messages in encryption. At least I'll be working on this more later.
    // fn chacha_protocol<T: Message>(mut p2pe: P2PEngine, other_protocol: impl Stream<Item = T>) {
    // TODO add flow for establishing a shared secret and encrypting  messages.
    // }
}

/// A simple proof-of-concept ping-pong protocol. Message responses have been disabled
/// Because the API is too wrapped up in itself to be usable.
pub mod pong {
    use crate::peer::{Message, P2PEngine, ParseMessageError, WILDCARD_ADDRESS};
    use futures::stream::select;
    use futures::stream::Stream;
    use futures::StreamExt;

    /// A basic message, without any data
    struct PingMessage {}

    /// This implementation shows how simple this API could be if I could make it expressive enough
    /// We just have to define a parser, an error, and a serializer for each specific kind of message.
    /// Here I decided to use two separate types to encode the two kinds of message, but a 'PingPongMessage'
    /// type with a boolean field would be more idiomatic as the pong_protocol below could just produce a stream
    /// of 'PingPongMessage's, for consumption by a user-facing library. Alas.  
    impl Message for PingMessage {
        fn parse(data: Vec<u8>) -> Result<Box<PingMessage>, ParseMessageError> {
            if data.len() == 1 && data[0] == 0 {
                Ok(Box::new(PingMessage {}))
            } else {
                Err(ParseMessageError("Not a valid ping message".to_string()))
            }
        }

        fn to_bytes(&self) -> Vec<u8> {
            return vec![0];
        }
    }

    /// The 'Pong' message corresponding to the ping above.
    struct PongMessage {}

    /// And it's implementation
    impl Message for PongMessage {
        fn parse(data: Vec<u8>) -> Result<Box<PongMessage>, ParseMessageError> {
            if data.len() == 1 && data[0] == 1 {
                Ok(Box::new(PongMessage {}))
            } else {
                Err(ParseMessageError("Not a valid pong message".to_string()))
            }
        }

        fn to_bytes(&self) -> Vec<u8> {
            return vec![1];
        }
    }

    /// This method should define the control flow for a ping-pong message stream.
    /// Unfortunately, I haven't designed a working message sending mechanism that fits
    /// with rust's ownership pattern, so this is half done. You can see the commented out
    /// flat_map() calls which should would have been used.
    pub fn pong_protocol(p2pe: &mut P2PEngine) -> impl Stream<Item = String> {
        select(
            p2pe.listen_for::<PingMessage>(WILDCARD_ADDRESS)
                //TODO: the message sending needs to be split off from the multiplexing I think.
                //I need to shrink the surface area even more... make the multiplexer a wrapper over a stream
                //Split off the message sending (which was always awkward) onto a separate object that can be
                // cloned and moved at will (probably using an Arc).
                // .flat_map(|m| p2pe.send(PongMessage{}, m.0).unwrap())
                .map(|_| "Ping!".to_string()),
            p2pe.listen_for::<PongMessage>(WILDCARD_ADDRESS)
                // .flat_map(|m| p2pe.send(PingMessage{}, m.0).unwrap())
                .map(|_| "Pong!".to_string()),
        )
    }
}
