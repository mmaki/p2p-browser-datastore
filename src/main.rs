// use bec_broadcast::peer::Peer;
use futures::executor::ThreadPool;
use futures::stream::{select, StreamExt};
use futures::task::SpawnExt;
use p2p_browser_datastore::peer::{P2PEngine, TransitMessage};
use p2p_browser_datastore::protocols::pong;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    //Define a new peer/multiplexer
    let mut peer1 = P2PEngine::new("Alice".to_string()).unwrap();
    let mut peer2 = P2PEngine::new("Bob".to_string()).unwrap();

    //Create some test messages
    let ping1 = TransitMessage {
        address: peer2.address(),
        value: vec![0],
    };
    let ping2 = TransitMessage {
        address: peer1.address(),
        value: vec![0],
    };

    //Create the protocols using the multiplexer
    let pong_1 = pong::pong_protocol(&mut peer1);
    let pong_2 = pong::pong_protocol(&mut peer2);

    let pool = ThreadPool::new()?;

    let (mut p1tx, _) = peer1.connect(pool.clone());
    let (mut p2tx, _) = peer2.connect(pool.clone());

    // There should be a proper UI implemented here which receives streams of 'ping!' and 'pong!' and
    // does something with them. I am simulating this by simply pumping the stream until nothing is left.
    pool.spawn(async move {
        let mut s = select(pong_1, pong_2).inspect(|s| println!("Made it to the end with: {}", s));

        //Pump the protocols
        while s.next().await.is_some() {
            continue;
        }
    })
    .unwrap();

    p1tx.try_send(ping1)?; //Let's see what happens
    p2tx.try_send(ping2)?;

    Ok(())
}
