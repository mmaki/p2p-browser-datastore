use ed25519_dalek::Keypair;
use futures::channel::mpsc;
use futures::channel::mpsc::{Receiver, Sender};
use futures::executor::ThreadPool;
use futures::future;
use futures::stream::{Stream, StreamExt};
use rand_core::OsRng;
use std::collections::HashMap;
use std::error::Error;
use std::fmt::{self, Debug};
use std::fs;

///The default storage location, TODO: Change this to use dirs
const DEFAULT_STORAGE_PATH: &str = "~/.p2p-db";
const KEY_FILE_NAME: &str = "ed25519.kp";
/// A special address which can be used to listen for messages from any source.
pub const WILDCARD_ADDRESS: [u8; 32] = [0; 32]; //the 256 bit value '0', should never be a valid public key

///A 256 bit number, usually a public key, used as the address format for now.
pub type U256 = [u8; 32];

///A handle for keeping track of sending channels
type ChannelId = u16;

///A connection agnostic multiplexer and public key storage. May have more features one day...
pub struct P2PEngine {
    id: String,
    keypair: Keypair,
    listeners: HashMap<U256, Vec<(ChannelId, Sender<TransitMessage>)>>,
    channel_counter: ChannelId,
    sender: Option<Sender<TransitMessage>>,
}

/// If parsing fails, use this error
#[derive(Debug)]
pub struct ParseMessageError(pub String);

//A message trait for parsing to and from a serialized format for messages
pub trait Message {
    fn parse(data: Vec<u8>) -> Result<Box<Self>, ParseMessageError>;
    fn to_bytes(&self) -> Vec<u8>;
}

/// A raw message in transmission. Address may either be a `to:` field or a `from:` field
/// depending on context. Outer code should simply ferry these packets to their destination
#[derive(Clone)]
pub struct TransitMessage {
    pub address: U256,
    pub value: Vec<u8>,
}

impl P2PEngine {
    /// Constructs a new P2PEngine instance, including reading and writing key pairs to files if need be.
    pub fn new(id: String) -> Result<P2PEngine, Box<dyn Error>> {
        let path = format!("{}/{}", DEFAULT_STORAGE_PATH, id);
        let file = format!("{}/{}", path, KEY_FILE_NAME);
        let keypair = match fs::read(&file) {
            Err(_) => {
                let kp = Keypair::generate(&mut OsRng {});
                fs::create_dir_all(path)?;
                fs::write(&file, kp.to_bytes())?;
                kp
            }
            Ok(file_contents) => Keypair::from_bytes(&file_contents)?,
        };

        Ok(P2PEngine {
            id,
            keypair,
            listeners: HashMap::new(),
            channel_counter: 0,
            sender: None,
        })
    }

    /// Get the short name of this instance
    pub fn name(&self) -> String {
        self.id.clone()
    }

    /// Get the address for this instance.
    pub fn address(&self) -> U256 {
        self.keypair.public.to_bytes()
    }

    /// Listen for messages, from a given host (or all hosts with the wildcard address)
    /// Returns a stream of messages from those hosts for processing.
    /// Generates a stream of (from_address, Box<Message>) pairs
    pub fn listen_for<T: Message>(&mut self, from: U256) -> impl Stream<Item = (U256, Box<T>)> {
        let (sender, receiver) = mpsc::channel(16);

        self.listeners
            .entry(from)
            .or_insert_with(Vec::new)
            .push((self.channel_counter, sender));

        self.channel_counter = self.channel_counter.wrapping_add(1);

        receiver
            .map(move |im| (im.address, T::parse(im.value)))
            .filter(|m| future::ready(m.1.is_ok()))
            .map(|m| (m.0, m.1.unwrap()))
    }

    //Sends a message to a given host and waits for a response back. Response message types should be uniquely
    //identifiable by parse(), as this method relies on this functionality to work
    pub fn send<T: Message>(
        &mut self,
        msg: T,
        to: U256,
    ) -> Result<impl Stream<Item = (U256, Box<T>)>, Box<dyn Error>> {
        //TODO silently add a nonce field and filter on that before delivering the message, so that parallel
        //sends() don't get their wires mixed up
        self.sender
            .as_mut()
            .expect("This instance is not connected somehow :(")
            .try_send(TransitMessage {
                address: to,
                value: msg.to_bytes(),
            })?;

        Ok(self.listen_for(to).take(1))
    }

    /// Consume the engine and return the communication streams to it.
    /// Should be called after all .listen_for()'s have been connected
    /// Takes a thread pool to spawn itself into
    ///
    /// This is the key insight for the next re-design, this entire functionality should be split
    /// off from the P2PEngine struct and should be a seperate multiplexer which takes in a stream
    /// of messages (from a TCP connection, a message queue, whatever), and produces multiple streams
    /// of output for different protocol-clients to use.
    /// Sending of messages should be in an entirely different component as well, as the ownership paths
    /// get twisted into knots otherwise.
    pub fn connect(
        mut self,
        pool: ThreadPool,
    ) -> (Sender<TransitMessage>, Receiver<TransitMessage>) {
        let (their_sender, our_receiver) = mpsc::channel(128);
        let (our_sender, their_receiver) = mpsc::channel(128);
        self.sender = Some(our_sender);

        let future = async move {
            //Pump the receiver
            let mut s = our_receiver.inspect(|m| {
                self.receive(m);
            });
            while s.next().await.is_some() {
                continue;
            }
        };

        pool.spawn_ok(future);

        (their_sender, their_receiver)
    }

    /// Receive a transit message from the great beyond and forwards it to listeners
    /// Also collects any dropped senders and removes them.
    /// All messages are delivered to the the wildcard address
    fn receive(&mut self, msg: &TransitMessage) {
        let mut closed: Vec<u16> = vec![];

        //Tentatively planning on adding support for address groupings later
        let addresses = vec![msg.address, WILDCARD_ADDRESS];

        for address in addresses {
            self.listeners.entry(address).and_modify(|senders| {
                for (handle, sender) in senders.iter_mut() {
                    if let Err(e) = sender.try_send(msg.clone()) {
                        if e.is_disconnected() {
                            closed.push(*handle);
                        }
                    }
                }
            });
        }

        if closed.is_empty() {
            self.listeners
                .entry(msg.address)
                .and_modify(|listeners| listeners.retain(|(handle, _)| !closed.contains(handle)));
        }
    }
}

///Format a public key into hex code, prints in little endian order
fn pk_to_string(data: &U256) -> String {
    let mut out = "".to_string();
    for byte in data {
        out = format!("{}{:02x}", out, byte);
    }
    out.replace("0x", "")
}

///Custom debug implementation to hide the private key.
impl Debug for P2PEngine {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Peer")
            .field("id", &self.id)
            .field("pk", &pk_to_string(&self.keypair.public.to_bytes()))
            .finish()
    }
}

#[test]
fn test_pk_to_string() {
    let mut data: [u8; 32] = [0; 32];
    assert_eq!(
        "0000000000000000000000000000000000000000000000000000000000000000",
        pk_to_string(&data)
    );
    data[0] = 1;
    assert_eq!(
        "0100000000000000000000000000000000000000000000000000000000000000",
        pk_to_string(&data)
    );
    data[0] = 15;
    assert_eq!(
        "0f00000000000000000000000000000000000000000000000000000000000000",
        pk_to_string(&data)
    );
    data[2] = 15;
    assert_eq!(
        "0f000f0000000000000000000000000000000000000000000000000000000000",
        pk_to_string(&data)
    );
}
